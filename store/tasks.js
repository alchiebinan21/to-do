export const useTaskStore = defineStore({
  id: "task-store",
  state: () => {
    return {
      tasks: [
        {
          id: 1,
          name: 'Task Done',
          status: {
            done: true
          }
        },
        {
          id: 2,
          name: 'Task Undone',
          status: {
            done: false
          }
        }
      ],
      counter: 2,
    }
  },
  persist: true,
  getters: {
    getTasks: (state) => {
      return state.tasks;
    }
  },
  actions: {
    updateTask(id) {
      const index = this.tasks.findIndex(task => task.id === id);
      this.tasks[index].status.done = !this.tasks[index].status.done;
    },
    addTask(name) {
      var task = {
        id: this.counter + 1,
        name,
        status: {
          done: false
        }
      }
      this.tasks.push(task);
      this.counter++;
    },
    clearTasks() {
      this.tasks = []
    },
    clearTasksDone() {
      this.tasks = this.tasks.filter(task => task.status.done !== true)
    },
    removeTask(id) {
      const index = this.tasks.findIndex(task => task.id === id);
      this.tasks.splice(index, 1);
    },
  }
})
